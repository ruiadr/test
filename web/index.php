<?php

require '../vendor/autoload.php';

use RandomLib\Factory;

$factory = new Factory();
$generator = $factory->getMediumStrengthGenerator();

echo $generator->generateString(32, 'abcdef');
